#
# /root/.bashrc
#

PS1='\u@\h \w $ '

if [ -r /etc/profile.d/docker-entrypoint-env.sh ]; then
    .  /etc/profile.d/docker-entrypoint-env.sh
fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/usr/local/sdkman"
[[ -s "/usr/local/sdkman/bin/sdkman-init.sh" ]] && source "/usr/local/sdkman/bin/sdkman-init.sh"

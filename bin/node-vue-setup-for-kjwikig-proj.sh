#!/bin/bash
#
#  nodejs vue ボイラーテンプレート
#
#  1. bash ./node-vue-setup-for-kjwikig-proj.sh  install  build ... 新規 npm モジュールインストール & npm build
#  2. bash ./node-vue-setup-for-kjwikig-proj.sh  ci       build ... package-lock.jsonに従ったバージョン指定インストール & npm build
#  3. bash ./node-vue-setup-for-kjwikig-proj.sh  install  none  ... 新規 npm モジュールインストールのみ実施。ビルドはしない。
#

set -e

function f-node-vue-setup-for-kjwikig-proj() {

    # デフォルト動作
    MODE=install
    MODE2=build

    # 引数解析
    if [ -n "$1" ]; then
        MODE=$1
    fi

    # 引数解析
    if [ -n "$2" ]; then
        MODE2=$2
    fi

    # install
    if [ x"$MODE"x = x"install"x ]; then

        # クリーニング
        rm -rf node_modules
        rm -rf package.json
        rm -rf package-lock.json

        set -x
        npm init --yes
        npm install --save vue
        npm install --save axios
        npm install --save lodash
        npm install --save-dev vue-property-decorator
        # 環境変数設定用
        npm install --save-dev cross-env
        # ディレクトリのお掃除用
        npm install --save-dev rimraf
        npm install --save-dev typescript
        # var mymodule = require("mymodule"); する時に使われる
        npm install --save-dev @types/node
        # ts-loaderはwebpackからtypescriptをコンパイルする時に使うカスタムコンパイラ
        npm install --save-dev webpack webpack-cli ts-loader
        # vue-loaderはwebpackからvueをコンパイルする時に使うカスタムコンパイラ
        # vue-template-compiler は .vueの中のテンプレートをコンパイルする時に使う
        npm install --save-dev vue-loader vue-template-compiler 
        # babel-loaderはwebpackからjsをコンパイルする時に使うカスタムコンパイラ
        npm install --save-dev babel-loader @babel/core @babel/preset-env
        # vueの中のcssを処理する時に使う。なぜかstyle-loaderが使えたｗｗ
        npm install --save-dev vue-style-loader css-loader style-loader
        # vueの中のcssを単独ファイルに抽出する際に使う webpack 4
        npm install --save-dev mini-css-extract-plugin
        npm install --save-dev eslint
        npm install --save-dev eslint-config-standard
        npm install --save-dev eslint-plugin-import
        npm install --save-dev eslint-plugin-node
        npm install --save-dev eslint-plugin-promise
        # 2020/12 明示的にインストールしなくても良くなった
        # npm install --save-dev eslint-plugin-standard
        npm install --save-dev eslint-plugin-vue

        npm install --save-dev @typescript-eslint/eslint-plugin
        npm install --save-dev @typescript-eslint/parser

        npm install --save-dev prettier
        # 2020年6月くらいに非推奨になった模様
        # npm install --save-dev prettier/eslint-config-prettier
        # npm install --save-dev prettier/eslint-plugin-prettier
        npm install --save-dev prettier-eslint
        # npm install --save-dev @typescript-eslint/eslint-plugin
        # npm install --save-dev @typescript-eslint/parser
        set +x

    fi

    # ci
    if [ x"$MODE"x = x"ci"x ]; then
        # クリーニング
        rm -rf node_modules

        # package-lock.jsonに従ってバージョン指定でパッケージをインストール
        npm ci
    fi


    # edit package.json npm scrits
    echo "modify package.json npm scripts"
cat > override-1.json << "EOF"
{
    "scripts": {
        "lint": "eslint src/main/typescript/**/*.ts",
        "lint-fix": "eslint --fix src/main/typescript/**/*.ts",
        "tsc": "tsc",
        "build": "webpack -d source-map",
        "dev": "webpack -d source-map --watch",
        "prod": "webpack --env prod --env min --env production --mode=production ",
        "build3": "cross-env NODE_ENV=development NODE_PATH=src ava"
    }
}
EOF
    yamlsort --input-output-file package.json --jsoninput --jsonoutput --override-file override-1.json
    rm override-1.json


    # edit package.json devDependencies
cat > override-2.json << "EOF"
{
  "main": "index.js"
}
EOF
    yamlsort --input-output-file package.json --jsoninput --jsonoutput --override-file override-2.json
    rm override-2.json


    # generate tsconfig.json sample
    if [ ! -f tsconfig.json ]; then
        ./node_modules/.bin/tsc --init
        mv tsconfig.json tsconfig.json.orig
    fi

#
# オレ手作りのtsconfig.json
#
if true ; then
# vue ファイルを処理するためには、shims-vue.d.ts が必用
echo "generate shims-vue.d.ts"
cat > shims-vue.d.ts << "EOF"
declare module '*.vue' {
    import Vue from 'vue'
    export default Vue
}
EOF
# tsconfig.jsonを作成
echo "generate tsconfig.json"
cat > tsconfig.json << "EOF"
{
    "compilerOptions": {
      "target": "es5",
      "module": "es2015",    /* umdだとなんか変になるので es2015とする。 */
      "importHelpers": true,
      "moduleResolution": "node",
  
      "strict": true,
      "noImplicitAny": false,                 /* Raise error on expressions and declarations with an implied 'any' type. */
      "strictNullChecks": false,              /* Enable strict null checks. */
      "strictFunctionTypes": false,           /* Enable strict checking of function types. */
      "strictBindCallApply": false,           /* Enable strict 'bind', 'call', and 'apply' methods on functions. */
      "strictPropertyInitialization": false,  /* Enable strict checking of property initialization in classes. */
      "noImplicitThis": false,                /* Raise error on 'this' expressions with an implied 'any' type. */

      "removeComments": false,
      "experimentalDecorators": true,
      "esModuleInterop": true,
      "allowSyntheticDefaultImports": true,
      "isolatedModules": false,
      "downlevelIteration": true,
      "sourceMap": true,
      "skipLibCheck": true,
      "declaration": true,
      "pretty": true,
      "newLine": "lf",
      "baseUrl": "./",
      "outDir": "./dist",
      "lib": [
        "esnext",
        "dom",
        "dom.iterable",
        "scripthost"
      ]
    },
    "files": [
      "shims-vue.d.ts"
    ],
    "include": [
        "./src/main/typescript/**/*.ts",
        "./src/main/typescript/**/*.tsx",
        "./src/main/typescript/**/*.vue"
    ],
    "exclude": [
        "node_modules"
    ]
}
EOF
fi


#
# webpack設定 手作り
#
echo "generate webpack.config.js"
cat > webpack.config.js << "EOF"
//
// for webpack 5
//

// output.pathに絶対パスを指定する必要があるため、pathモジュールを読み込んでおく
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {

  // モード値を production に設定すると最適化された状態で、
  // development に設定するとソースマップ有効でJSファイルが出力される
  mode: 'development', // "production" | "development" | "none"

  // メインとなるJavaScriptファイル（エントリーポイント）
  entry: './src/main/typescript/main.ts',

  // 出力先
  output: {
    path: path.join(__dirname, "src/main/webapp/js/"),
    filename: "output.js"
  },

  module: {
    rules: [
      // 拡張子.vueのファイルに対する設定
      {
        test: /\.vue$/,
        loader: "vue-loader"
      },

      // 拡張子 .ts の場合
      {
        test: /\.ts$/,
        // TypeScript をコンパイルする
        loader: 'ts-loader',
        // .vueに対しても TypeScriptを使用する
        options: {
          appendTsSuffixTo: [/\.vue$/]
        }
      },

      // 拡張子 .cssのファイルに対する指定
      {
        test: /\.css$/,
        use: [
          // useの配列は後ろから実行されるので注意。
          // 'vue-style-loader' を指定するのが本当だが・・。
          // 手元の環境ではなぜか 'style-laoder' の方が動くのでこっちを採用ｗｗ
          'style-loader',
          // 'vue-style-loader',
          {
            loader: 'css-loader',
            options: {
              // enable CSS Modules
              // modules: true,
              // disable url(hoge.css)
              url: false
            }
          }
        ]
      },

      // 拡張子.jsのファイルに対する設定
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          },
        ]
      },

    ]
  },
  // import 文で .ts ファイルを解決するため
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js', // 完全ビルド(されたVue.js)を読み込む
    },
    modules: [
      "node_modules", // node_modules 内も対象とする
    ],
    extensions: [
      '.ts',
      '.js', // node_modulesのライブラリ読み込みに必要
      '.vue'
    ]
  },
  // プラグインを列挙。ここに書いておくと、毎回 import しなくて良くなる。
  plugins: [
    new VueLoaderPlugin(),
  ],
};
EOF


#
# eslint 設定 手作り
#
# https://github.com/prettier/prettier/blob/master/docs/integrating-with-linters.md
# 2020/12/31 prettier-eslint を使った場合、 eslint設定に prettier を明示的に設定する必用は無くなる
#
echo "generate eslint setting .eslintrc.js"
cat > .eslintrc.js << "EOF"
module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: [
    'plugin:vue/essential',
    // 'standard',
    // 'prettier',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    parser: '@typescript-eslint/parser',
    project: "./tsconfig.json",
    sourceType: 'module'
  },
  plugins: [
    // 'prettier',
    'vue',
    '@typescript-eslint'
  ],
  rules: {
    // シングルクォートは必須ではない。セミコロンは必須。
    // "prettier/prettier": ["error", {"singleQuote": false, "semi": true}]
    // "prettier/prettier": ["error", {  }]
  }
}
EOF


#
# サンプルソース投入
#
mkdir -p ./src/main/webapp
if [ ! -f ./src/main/webapp/index.html ] ; then
cat > ./src/main/webapp/index.html << "EOF"
<html>
<head>
<title>node works!</title>
<script type="text/javascript" src="./js/output.js"></script>
</head>
<body>
<h1>node works!</h1>
<script type="text/javascript">
var msg = samplefunc("hoge");
console.log(msg);
</script>
</body>
</html>
EOF
fi

mkdir -p ./src/main/typescript
if [ ! -f ./src/main/typescript/samplefunc.ts ]; then
cat > ./src/main/typescript/samplefunc.ts << "EOF"
export function samplefunc(argstr : string) : string {
    return "hello " + argstr;
}
EOF
fi

#
# サンプルソース投入
#
mkdir -p ./src/main/typescript

if [ ! -f ./src/main/typescript/main.ts ] ; then
cat > ./src/main/typescript/main.ts << "EOF"
/*
 * main.ts
 *
 * webpack の entry。
 */

import Vue from "vue";
import axios from "axios";

var webchat = require("./webchat");
function createWebSocketVueControler(argDocumentLocationObj, argAppName, argAppContextPath, argMountElementQuery, argTextareaE
lementQuery, argAppPictBbsUserName) {
    webchat.createWebSocketVueControler(argDocumentLocationObj, argAppName, argAppContextPath, argMountElementQuery, argTextar
eaElementQuery, argAppPictBbsUserName);
}

var imagePaste = require("./imagePaste");
function createVueControler( argDocumentLocationObj, argAppName, argAppContextPath, argMountElementQuery, argTextareaElementQu
ery) {
    imagePaste.createVueControler( argDocumentLocationObj, argAppName, argAppContextPath, argMountElementQuery, argTextareaEle
mentQuery);
}

var createPage = require("./createPage");
function createVueForNewPage(argDocumentLocationObj, argAppName, argAppContextPath, argAppUserName) {
    createPage.createVueForNewPage(argDocumentLocationObj, argAppName, argAppContextPath, argAppUserName);
}

/*
 * end of file
 */
EOF
fi

# 型定義ファイル投入
if [ ! -f ./src/main/typescript/vue-prototype.d.ts ] ; then
cat > ./src/main/typescript/vue-prototype-config.d.ts << "EOF"
//
// Vue.prototype.$xxxx の型定義ファイルを用意
//
// 結局、外部のscriptからはVue.prototypeが見えなかったので使用せず。
//
import Vue from "vue";
declare module "vue/types/vue" {
  interface Vue {
    $dummy: string;
  }
}
EOF
fi

# window.xxx に追加してグローバルにする小技
if [ ! -f ./src/main/typescript/window.ts ] ; then
cat > ./src/main/typescript/window.ts << "EOF"
// 割と外道な定義。 window に Vue を追加することで、外部のJavaScriptから使えるglobalなModuleになる
// https://sansaisoba.qrunch.io/entries/NrmnZGUHE3xZhO3a TypeScriptでwindowにプロパティを追加する - sansaisoba's tech blog

import Vue from "vue";

// window.ts
interface MyWindow extends Window {
  Vue: Vue;
  axios: any;
  createWebSocketVueControler: any;
  createVueControler: any;
  createVueForNewPage: any;
}
declare var window: MyWindow;
export default window;
EOF
fi


    echo ""
    echo "--------------------------------"
    echo "environment setup npm $MODE SUCCESS."
    echo "--------------------------------"
    echo ""

    if [ -z "$MODE2" ]; then
        echo "  how to use npm scripts"
        echo "    npm run tsc        (tsc)"
        echo "    npm run build      (webpack -d)"
        echo "    npm run prod       (webpack -p)"
        echo "    npm run lint       (eslint)"
        echo "    npm run lint-fix   (eslint --fix)"
        echo ""
    elif [ x"$MODE2"x = x"none"x ]; then
        echo "skip npm run build. "
    elif [ -n "$MODE2" ]; then
        echo ""
        echo "--------------------------------"
        echo "num run $MODE2"
        echo "--------------------------------"
        echo ""
        npm run $MODE2
    fi


}

f-node-vue-setup-for-kjwikig-proj "$@"


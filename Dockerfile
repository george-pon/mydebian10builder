FROM registry.gitlab.com/george-pon/mydebian10docker:latest

ENV MYDEBIAN10BUILDER_VERSION build-target
ENV MYDEBIAN10BUILDER_VERSION latest
ENV MYDEBIAN10BUILDER_VERSION stable
ENV MYDEBIAN10BUILDER_IMAGE registry.gitlab.com/george-pon/mydebian10builder

ENV DEBIAN_FRONTEND noninteractive

# install OpenJDK
RUN apt update -y && apt install -y openjdk-11-jdk

# install gradle
# RUN apt install -y gradle

# install SDKMAN
# https://sdkman.io/ Home - SDKMAN! the Software Development Kit Manager
# install Gradle 6
# https://gradle.org/install/ Gradle | Installation
RUN curl -s "https://get.sdkman.io" > sdkman-install.sh && \
    export SDKMAN_DIR=/usr/local/sdkman && \
    bash sdkman-install.sh && \
    bash -c "source /usr/local/sdkman/bin/sdkman-init.sh && sdk install gradle 6.4"

# install node.js Using Debian, as root
RUN curl -sL https://deb.nodesource.com/setup_15.x | bash - && \
    apt-get install -y nodejs

ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ADD bashrc /root/.bashrc
ADD bash_profile /root/.bash_profile
ADD vimrc /root/.vimrc
ADD emacsrc /root/.emacs
ADD bin /usr/local/bin
RUN chmod +x /usr/local/bin/*.sh

ENV HOME /root
ENV ENV $HOME/.bashrc

# setup git and npm
# RUN bash -x /usr/local/bin/debian10builder-init-container.sh

CMD ["/usr/local/bin/docker-entrypoint.sh"]

